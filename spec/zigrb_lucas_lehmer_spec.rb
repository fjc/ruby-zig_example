# frozen_string_literal: true

require_relative 'spec_helper'

RSpec.describe ZigExample do
  it 'has a version number' do
    expect(ZigExample::VERSION).not_to be nil
  end

  it 'does something useful' do
    expect(ZigExample.new.lucas_lehmer(1)).to eq(false)
    expect(ZigExample.new.lucas_lehmer(2)).to eq(true)
    expect(ZigExample.new.lucas_lehmer(3)).to eq(true)
    expect(ZigExample.new.lucas_lehmer(4)).to eq(false)
    expect(ZigExample.new.lucas_lehmer(5)).to eq(true)
    expect(ZigExample.new.lucas_lehmer(11)).to eq(false)
  end
end
