# frozen_string_literal: true

require_relative 'spec_helper'

RSpec.describe ZigExample do
  it 'has a version number' do
    expect(ZigExample::VERSION).not_to be nil
  end

  it 'does something useful' do
    expect(ZigExample.new.hundred_doors(1)).to eq(100)
    expect(ZigExample.new.hundred_doors(2)).to eq(50)
    expect(ZigExample.new.hundred_doors(4)).to eq(58)
    expect(ZigExample.new.hundred_doors(8)).to eq(46)
  end
end
