# frozen_string_literal: true

require_relative 'spec_helper'

RSpec.describe ZigExample do
  it 'has a version number' do
    expect(ZigExample::VERSION).not_to be nil
  end

  it 'does something useful' do
    expect(ZigExample.new.ack(0,0)).to eq(1)
    expect(ZigExample.new.ack(3,4)).to eq(125)
  end
end
