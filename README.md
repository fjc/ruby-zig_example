An example gem for ruby with native zig extensions.  Anything of value
here is derived from:

* https://zenn.dev/thinkingsinc/articles/zig-and-rubygems
* https://katafrakt.me/2022/12/25/ruby-extension-zig/

Originally cloned from:

* https://github.com/katafrakt/zig-ruby
* https://github.com/kazto/mkmf-zig

Requires a zig release newer than 0.11 with the build system changes:

* https://devlog.hexops.com/2023/zig-0-11-breaking-build-changes/

Installation:

* `gem install [--user] gem-ext-zig_builder`
* `gem install [--user] zig_example`
* or, `RUBYOPT=-rgem/ext/zig_builder gem install [--user] zig_example`

Zig extensions:

* [100 doors](https://rosettacode.org/wiki/100_doors#Zig)
  * Examples
    * `ZigExample.new.hundred_doors( 7)` => 52
    * `ZigExample.new.hundred_doors(77)` => 29
* [Ackermann function](https://rosettacode.org/wiki/Ackermann_function#Zig)
  * Examples
    * `ZigExample.new.ack(0, 0)` =>   1
    * `ZigExample.new.ack(3, 4)` => 125
* [Lucas-Lehmer test](https://rosettacode.org/wiki/Lucas-Lehmer_test#C)
  * This example actually uses an implementation in C, but using the
    Zig compiler, build system, and integration glue.  The C file is
    first built as a static lib w/ pic.  Then the Zig wrapper is built
    and linked with it.
  * Examples
    * `ZigExample.new.ack(11)` => false
    * `ZigExample.new.ack(13)` => true

|   Benchmark   |    Mode     |   Ruby vs Zig   |
| ------------- | ----------- | --------------: |
| hundred_doors | Debug       |  12.75x  slower |
| hundred_doors | ReleaseSafe | 106.97x  slower |
| hundred_doors | ReleaseFast | 137.69x  slower | 
| ackermann     | Debug       |   5.26x  slower |
| ackermann     | ReleaseSafe |  29.19x  slower |
| ackermann     | ReleaseFast |  42.27x  slower |

Example run:

```
$ zig version
0.11.0-dev.2646+3d33a0906

$ ruby -v
ruby 2.7.4p191 (2021-07-07 revision a21a3b7d23) [x86_64-linux-gnu]

$ rake spec
....
Finished in 0.04283 seconds (files took 0.30687 seconds to load)
4 examples, 0 failures

$ rake benchmark
Ruby: 10, Zig: 10
Ruby: 125, Zig: 125
Warming up --------------------------------------
                Ruby   688.000  i/100ms
                 Zig    81.901k i/100ms
Calculating -------------------------------------
                Ruby      7.559k (±23.9%) i/s -     35.088k in   5.016890s
                 Zig    808.540k (±20.9%) i/s -      3.931M in   5.089411s
Comparison:
                 Zig:   808539.8 i/s
                Ruby:     7558.8 i/s - 106.97x  slower
Warming up --------------------------------------
                Ruby    92.000  i/100ms
                 Zig     2.602k i/100ms
Calculating -------------------------------------
                Ruby      1.156k (±26.4%) i/s -      5.428k in   5.074223s
                 Zig     33.752k (±34.8%) i/s -    150.916k in   5.044444s
Comparison:
                 Zig:    33752.1 i/s
                Ruby:     1156.4 i/s - 29.19x  slower

$ rake build
zig_example 0.2.0 built to pkg/zig_example-0.2.0.gem.

$ gem install --user pkg/zig_example-0.2.0.gem
Building native extensions. This could take a while...
Successfully installed zig_example-0.2.0
Parsing documentation for zig_example-0.2.0
Installing ri documentation for zig_example-0.2.0
Done installing documentation for zig_example after 3 seconds
1 gem installed

$ ruby -rzig_example -e 'p ZigExample.new.hundred_doors(7)'
52

$ ruby -rzig_example -e 'p ZigExample.new.hundred_doors(77)'
29

$ ruby -rzig_example -e 'p ZigExample.new.hundred_doors(777)'
thread 40121 panic: integer overflow
.../ext/zigrb_100doors/src/main.zig:12:36: 0x71239153d728 in hundred_doors (zigrb_100doors)
    while (pass <= passes) : (pass += 1) {
                                   ^
.../ext/zigrb_100doors/src/main.zig:38:38: 0x71239153ce8a in rb_hundred_doors (zigrb_100doors)
    return ruby.INT2NUM(hundred_doors(passes));
                                     ^
Aborted

$ ruby -rzig_example -e 'p ZigExample.new.ack(0,0)'
1

$ ruby -rzig_example -e 'p ZigExample.new.ack(3,4)'
125
```

Benchmark (-Doptimize=Debug):
```
$ rake benchmark
Warming up --------------------------------------
                Ruby   678.000  i/100ms
                 Zig     6.767k i/100ms
Calculating -------------------------------------
                Ruby      5.796k (±36.5%) i/s -     25.764k in   5.008031s
                 Zig     73.909k (±36.3%) i/s -    331.583k in   5.075641s

Comparison:
                 Zig:    73908.8 i/s
                Ruby:     5795.6 i/s - 12.75x  slower

Warming up --------------------------------------
                Ruby    91.000  i/100ms
                 Zig   292.000  i/100ms
Calculating -------------------------------------
                Ruby    949.940  (±26.2%) i/s -      4.459k in   5.078054s
                 Zig      5.000k (±44.3%) i/s -     20.148k in   5.007243s

Comparison:
                 Zig:     4999.7 i/s
                Ruby:      949.9 i/s - 5.26x  slower
```

Benchmark (-Doptimize=ReleaseFast):
```
Warming up --------------------------------------
                Ruby   645.000  i/100ms
                 Zig   101.974k i/100ms
Calculating -------------------------------------
                Ruby      7.284k (±23.3%) i/s -     34.830k in   5.069010s
                 Zig      1.003M (±24.3%) i/s -      4.691M in   5.083505s

Comparison:
                 Zig:  1003006.7 i/s
                Ruby:     7284.3 i/s - 137.69x  slower

Warming up --------------------------------------
                Ruby    87.000  i/100ms
                 Zig     4.970k i/100ms
Calculating -------------------------------------
                Ruby      1.148k (±27.0%) i/s -      5.394k in   5.059366s
                 Zig     48.541k (±20.0%) i/s -    238.560k in   5.106795s

Comparison:
                 Zig:    48540.8 i/s
                Ruby:     1148.3 i/s - 42.27x  slower
```
