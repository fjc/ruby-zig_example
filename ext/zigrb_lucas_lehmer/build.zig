const std = @import("std");

pub fn build(b: *std.Build) void {
    const optimize = b.standardOptimizeOption(.{});
    const target = b.standardTargetOptions(.{});

    ////////////////////////////////////////////////////////////////
    // lib

    const clib = b.addStaticLibrary(.{
        .name = "zigrb_lucas_lehmer",
        .root_source_file = .{ .path = "src/lucas_lehmer.c" },
        .target = target,
        .optimize = optimize,
    });
    clib.force_pic = true;

    clib.linkSystemLibrary("gmp");

    const lib = b.addSharedLibrary(.{
        .name = "zigrb_lucas_lehmer",
        .root_source_file = .{ .path = "src/wrapper.zig" },
        .target = target,
        .optimize = optimize,
    });

    lib.linkSystemLibrary(std.os.getenv("RUBY_PC") orelse "ruby");
    lib.linkSystemLibrary("c");

    lib.addIncludePath("src");
    lib.linkLibrary(clib);

    b.installArtifact(lib);

    ////////////////////////////////////////////////////////////////
    // test

    const unit_tests = b.addTest(.{
        .root_source_file = .{ .path = "src/wrapper.zig" },
        .target = target,
        .optimize = optimize,
    });

    unit_tests.linkSystemLibrary(std.os.getenv("RUBY_PC") orelse "ruby");
    unit_tests.linkSystemLibrary("c");

    unit_tests.addIncludePath("src");
    unit_tests.linkLibrary(clib);

    const run_unit_tests = b.addRunArtifact(unit_tests);
    const test_step = b.step("test", "Run unit tests");
    test_step.dependOn(&run_unit_tests.step);
}
