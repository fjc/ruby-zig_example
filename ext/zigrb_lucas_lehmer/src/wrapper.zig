const std = @import("std");
const testing = std.testing;

const ruby = @cImport(@cInclude("ruby/ruby.h"));
const clib = @cImport(@cInclude("lucas_lehmer.h"));

fn rb_lucas_lehmer(...) callconv(.C) ruby.VALUE {
    var ap = @cVaStart();
    defer @cVaEnd(&ap);

    // first argument is `self` in ruby; discard it
    var self = @cVaArg(&ap, ruby.VALUE); _ = self;

    // back and forth type conversions + delegation
    var p = ruby.NUM2ULONG(@cVaArg(&ap, ruby.VALUE));
    return if (clib.lucas_lehmer(p) == 1) ruby.Qtrue else ruby.Qfalse;
}

export fn Init_libzigrb_lucas_lehmer() void {
    ruby.ruby_init();
    var zig_rb_class: ruby.VALUE = ruby.rb_define_class("ZigExample", ruby.rb_cObject);
    _ = ruby.rb_define_method(zig_rb_class, "lucas_lehmer", rb_lucas_lehmer, 1);
}

test "lucas_lehmer 1 passes" {
    try testing.expect(clib.lucas_lehmer(1) == 0);
}
test "lucas_lehmer 2 passes" {
    try testing.expect(clib.lucas_lehmer(2) == 1);
}
test "lucas_lehmer 3 passes" {
    try testing.expect(clib.lucas_lehmer(3) == 1);
}
test "lucas_lehmer 4 passes" {
    try testing.expect(clib.lucas_lehmer(4) == 0);
}
test "lucas_lehmer 5 passes" {
    try testing.expect(clib.lucas_lehmer(5) == 1);
}
test "lucas_lehmer 11 passes" {
    try testing.expect(clib.lucas_lehmer(11) == 0);
}
