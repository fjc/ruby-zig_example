const std = @import("std");

pub fn build(b: *std.Build) void {
    const optimize = b.standardOptimizeOption(.{});
    const target = b.standardTargetOptions(.{});

    ////////////////////////////////////////////////////////////////
    // lib

    const lib = b.addSharedLibrary(.{
        .name = "zigrb_ackermann",
        .root_source_file = .{ .path = "src/main.zig" },
        .target = target,
        .optimize = optimize,
    });

    lib.linkSystemLibrary(std.os.getenv("RUBY_PC") orelse "ruby");
    lib.linkSystemLibrary("c");

    b.installArtifact(lib);

    ////////////////////////////////////////////////////////////////
    // test

    const unit_tests = b.addTest(.{
        .root_source_file = .{ .path = "src/main.zig" },
        .target = target,
        .optimize = optimize,
    });

    unit_tests.linkSystemLibrary(std.os.getenv("RUBY_PC") orelse "ruby");
    unit_tests.linkSystemLibrary("c");

    const run_unit_tests = b.addRunArtifact(unit_tests);
    const test_step = b.step("test", "Run unit tests");
    test_step.dependOn(&run_unit_tests.step);
}
