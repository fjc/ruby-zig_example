# frozen_string_literal: true

require_relative 'lib/zig_example/version'

Gem::Specification.new do |spec|
  spec.name = 'zig_example'
  spec.version = ZigExample::VERSION
  spec.authors = ['Frank J. Cameron']
  spec.email = ['fjc@fastmail.net']

  spec.summary = 'An example gem for ruby with native zig extensions (relies on gem-ext-zig_builder).'
  spec.homepage = 'https://gitlab.com/fjc/ruby-zig_example'
  spec.license = 'MIT'
  spec.required_ruby_version = '>= 2.6.0'

  spec.metadata['homepage_uri'] = spec.homepage
  spec.metadata['source_code_uri'] = spec.homepage

  spec.files = Dir['lib/**/*.rb'] + Dir['ext/**/*.zig'] + Dir['ext/**/*.[ch]']
  spec.files.reject! { |fn| fn.include? 'zig-cache' }
  spec.files.reject! { |fn| fn.include? 'zig-out' }
  spec.require_paths = ['lib']
  spec.extensions = [
    'ext/zigrb_100doors/build.zig',
    'ext/zigrb_ackermann/build.zig',
    'ext/zigrb_lucas_lehmer/build.zig',
  ]
end
