# frozen_string_literal: true

require_relative 'zig_example/version'

%w{
  zigrb_100doors
  zigrb_ackermann
  zigrb_lucas_lehmer
}.each do |lib|
  begin
    require "lib#{lib}"
  rescue LoadError
    require_relative "../ext/#{lib}/lib#{lib}"
  end
end
