require 'bundler/inline'

gemfile do
  source 'https://rubygems.org'
  gem 'benchmark-ips'
end

def hundred_doors(passes)
  doors = Array.new(101, false)
  passes.times do |i|
    i += 1
    (i..100).step(i) do |d|
      doors[d] = !doors[d]
    end
  end
  # dropping first one as it does not count
  doors.drop(1).count {|d| d}
end

require 'benchmark/ips'

require_relative 'lib/zig_example'
zig = ZigExample.new

####

puts "Ruby: #{hundred_doors(100)}, Zig: #{zig.hundred_doors(100)}"

Benchmark.ips do |x|
  x.report('Ruby') { hundred_doors(100) }
  x.report('Zig') { zig.hundred_doors(100) }
  x.compare!
end

####

def ack(m, n)
  if m == 0
    n + 1
  elsif n == 0
    ack(m-1, 1)
  else
    ack(m-1, ack(m, n-1))
  end
end

puts "Ruby: #{ack(3, 4)}, Zig: #{zig.ack(3, 4)}"
Benchmark.ips do |x|
  x.report('Ruby') { ack(3, 4) }
  x.report('Zig') { zig.ack(3, 4) }
  x.compare!
end

####

def is_prime ( p )
  return true  if p == 2
  return false if p <= 1 || p.even?
  (3 .. Math.sqrt(p)).step(2) do |i|
    return false  if p % i == 0
  end
  true
end
def is_mersenne_prime ( p )
  return true  if p == 2
  m_p = ( 1 << p ) - 1
  s = 4
  (p-2).times { s = (s ** 2 - 2) % m_p }
  s == 0
end
def lucas_lehmer ( p )
  is_prime(p) && is_mersenne_prime(p)
end

[2, 107, 11213].each do |p|
  puts "Ruby: #{lucas_lehmer(p)}, ZigC: #{zig.lucas_lehmer(p)}"
  Benchmark.ips do |x|
    x.report('Ruby') { lucas_lehmer(p) }
    x.report('ZigC') { zig.lucas_lehmer(p) }
    x.compare!
  end
end

####
